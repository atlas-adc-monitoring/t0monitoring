#! /usr/bin/python3
import os
import json
from datetime import datetime, timezone
import logging
import pprint
import pyAMI.client
import pyAMI.atlas.api as AtlasAPI

if os.path.exists('/data/atlcrc/'):
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/t0/t0ttc.log', filemode='w', format='%(message)s', level=logging.INFO)
  outputPath = '/eos/home-a/atlcrc/www/t0/'
else:
  logging.basicConfig(filename='t0ttc.log', filemode='w', format='%(message)s', level=logging.INFO)
  outputPath = ''

logging.info("pyami script started: "+str(datetime.now()))

client = pyAMI.client.Client('atlas-replica')
AtlasAPI.init()

#print(AtlasAPI.list_files(client, 'data22_cos.00414365.physics_IDCosmic.merge.RAW'))
#print(AtlasAPI.get_dataset_info(client, 'data22_cos.00414365.physics_IDCosmic.merge.RAW'))
#print(AtlasAPI.get_dataset_prov(client, 'data22_cos.00414365.physics_IDCosmic.merge.RAW'))
#print(AtlasAPI.list_datasets(client, patterns = ['%414365%'], fields = ['ldn', 'dataset_number', 'prodsys_status', 'nfiles'], limit = [1, 10], type = 'AOD'))
#field: display additional fields (comma separated parameter, available fields: ami_status, ami_tags, atlas_release, beam, completion, conditions_tag, cross_section, dataset_number, ecm_energy, events, files.cross_section, files.events,files.generator_filter_efficienty, files.guid, files.input_file, files.lfn, files.size, generator_filter_efficienty, generator_name, generator_tune, geometry, hashtags.comment, hashtags.fullname, hashtags.name, hashtags.scope, in_container, job_config, keywords.name, ldn, modified, nfiles, pdf, period, physics_comment, physics_short, prodsys_status, production_step, project, requested_by, responsible, run_number, stream, total_size, transformation_package, trash_annotation, trash_date, trash_trigger, trigger_config, type)
#
Year = int(datetime.now(tz=timezone.utc).strftime("%Y"))
periodList = []
try:
  periods = AtlasAPI.list_dataperiods(client, 2, year=Year)#level - 1 is C1, B2,...; 2 is A, B, C,...
  for p in periods:
    for k,v in p.items():
      if k == 'period':
        if v not in ['VdM','VDM']:
          periodList.append(v)
  #print(sorted(periodList, reverse=True))
  y = int(datetime.now(tz=timezone.utc).strftime("%y"))
  runList = []
  runs = AtlasAPI.list_runs(client, year=y, data_periods=sorted(periodList, reverse=True)[0])#taking only runs from the last period
  for r in runs:
    for k,v in r.items():
      if k == 'runNumber':
        runList.append(v)
  #print(runList)
  datasetOList = []
  for rl in runList:
    pattern = 'data'+str(y)+'%eV%'+str(rl)+'%'
    #print(pattern)
    datasets = AtlasAPI.list_datasets(client, patterns = [pattern], limit = [1, 1000])#get list of datasets with high limit - there are tens of streams and each can have tens of formats
    for d in datasets:
      for k,v in d.items():
        if 'physics_Main' in v:
          datasetOList.append(d)
  #print(datasetOList)
  out = []
  for d in datasetOList:
    dinfo = AtlasAPI.get_dataset_info(client,d['ldn'])
    temp = {}
    if 'DAOD_PHYS.' in d['ldn']:
      prov = AtlasAPI.get_dataset_prov(client,d['ldn'])
      for i in prov['edge']:
        if 'destination' in list(i) and d['ldn'] == i['destination']:
          temp['provAOD'] = i['source']
    temp['ldn'] = dinfo[0]['logicalDatasetName']
    temp['run_number'] = dinfo[0]['runNumber']
    temp['nfiles'] = dinfo[0]['nFiles']
    temp['nevents'] = dinfo[0]['totalEvents']
    temp['created'] = dinfo[0]['created']
    out.append(temp)
  #print(out)
except pyAMI.exception.Error as e:
  out = []
  logging.info("getting data from pyami failed with:")
  logging.info("------------------------------------")
  logging.info(str(e))
  logging.info("------------------------------------")


if os.path.exists(outputPath+'sent.json'):
  with open(outputPath+'sent.json') as sf:
    sent = json.load(sf)
else:
  sent = {}

if os.path.exists(outputPath+'unfinished.json'):
  with open(outputPath+'unfinished.json') as uf:
    unfinished = json.load(uf)
else:
  unfinished = {}

#removing entire runs which were already sent
notSent = []
for p in out:
  if p['run_number'] not in list(sent):
    notSent.append(p)
#print(notSent)

#for unfinished runs (i.e. those not yet sent), check per dataset and merge the data
for i in notSent:
  if i['run_number'] not in list(unfinished):
    unfinished[i['run_number']] = {}
    if i['ldn'] not in list(unfinished[i['run_number']]):
      unfinished[i['run_number']][i['ldn']] = i
  else:
    if i['ldn'] not in list(unfinished[i['run_number']]):
      unfinished[i['run_number']][i['ldn']] = i
#pprint.pprint(unfinished)

with open(outputPath+'Py.json', 'w') as outfile:
  json.dump(unfinished, outfile)
  
logging.info("output to store in: https://atlasdistributedcomputing-live.web.cern.ch/t0/Py.json")
logging.info("pyami script finished: "+str(datetime.now()))
