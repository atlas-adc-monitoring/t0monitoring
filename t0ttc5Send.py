#! /usr/bin/python3
#send final data to monit
import os
import json
import requests
from datetime import datetime, timezone
import logging
import pprint

if os.path.exists('/data/atlcrc/'):
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/t0/t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = '/eos/home-a/atlcrc/www/t0/'
else:
  logging.basicConfig(filename='t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = ''

logging.info("sending script started: "+str(datetime.now()))
Year = int(datetime.now(tz=timezone.utc).strftime("%Y"))

def checkCondition(word, data, firstDaod):#checking if the variable is defined and not empty
  if firstDaod in list(data):
    if word in list(data[firstDaod]) and len(str(data[firstDaod][word])) > 0:
      return True
    else:
      return False
  else:
    if word in list(data) and len(str(data[word])) > 0:
      return True
    else:
      return False

def monit(rn):
  #pprint.pprint(amibppbcatr[rn])
  report = {}
  report['producer'] = 't0ttc'
  report['type'] = 't0ttc'
  report['run'] = 3
  report['year'] = int(Year)
  report['run_number'] = int(rn)
  ecm = False#to check if EOR, CTIME, and MTIME are defined in data
  raw = False
  aod = False
  daod = False
  if checkCondition('EOR', amibppbcatr[rn], '') and checkCondition('ctime', amibppbcatr[rn], '') and checkCondition('mtime', amibppbcatr[rn], ''):
    ecm = True
    report['eor'] = int(amibppbcatr[rn]['EOR'])*1000
    report['ctime'] = int(amibppbcatr[rn]['ctime'])*1000
    report['mtime'] = int(amibppbcatr[rn]['mtime'])*1000
    report['timestamp'] = report['eor']
    report['_id'] = str(report['timestamp'])+'_'+str(report['run_number'])
  for d in amibppbcatr[rn]:
    if '.RAW' in d:
      if checkCondition('transferend', amibppbcatr[rn], d) and checkCondition('nevents', amibppbcatr[rn], d):
        raw = True
        report['raw2T1'] = int(amibppbcatr[rn][d]['transferend'])*1000
        report['nevents'] = int(amibppbcatr[rn][d]['nevents'])
  #find first DAOD_PHYS that finished and report its values/provenance
  daodEnd = {}#{taskend:dataset}
  for d in amibppbcatr[rn]:
    if '.DAOD_PHYS.' in d:
      if checkCondition('taskend', amibppbcatr[rn], d):
        daodEnd[amibppbcatr[rn][d]['taskend']] = d
  if len(daodEnd) > 0:
    firstDaod = daodEnd[min(list(daodEnd))]
  else:
    firstDaod = ''
  if checkCondition('taskstart', amibppbcatr[rn], firstDaod) and checkCondition('taskend', amibppbcatr[rn], firstDaod) and checkCondition('provAOD', amibppbcatr[rn], firstDaod):
    daod = True
    report['taskstart'] = int(amibppbcatr[rn][firstDaod]['taskstart'])*1000
    report['taskend'] = int(amibppbcatr[rn][firstDaod]['taskend'])*1000
    firstDaodAod = amibppbcatr[rn][firstDaod]['provAOD']
  if checkCondition('transferend', amibppbcatr[rn], firstDaodAod):
    aod = True
    report['aod2T1'] = int(amibppbcatr[rn][firstDaodAod]['transferend'])*1000
  if ecm and raw and aod and daod:#if all values are defined return the report, otherwise return empty
    return report
  else:
    #print(ecm)
    #print(raw)
    #print(aod)
    #print(daod)
    #print(report)
    #pprint.pprint(amibppbcatr[rn])
    return {}

if os.path.exists(outputPath+'PyBpPbCaTr.json'):
  with open(outputPath+'PyBpPbCaTr.json') as f2:
    amibppbcatr = json.load(f2)
else:
  amibppbcatr = {}

#get finished runs
rnList = []
for r in list(amibppbcatr):
  for d in list(amibppbcatr[r]):
    if '.DAOD_PHYS.' in d:
      if 'taskend' in list(amibppbcatr[r][d]):
        if r not in rnList:
          rnList.append(r)
#print(rnList)

#sending to monit metrics
monitreport = []
for i in rnList:
  rr = monit(i)
  if len(rr) > 0:
    monitreport.append(rr)
#pprint.pprint(monitreport)
reallySentRuns = []
#run numbers which were really sent
for k in monitreport:
  if 'run_number' in list(k):
    reallySentRuns.append(k['run_number'])
logging.info(pprint.pformat(monitreport))
if os.path.exists('/data/atlcrc/'):
  response = requests.post('http://monit-metrics:10012/', data=json.dumps(monitreport), headers={ "Content-Type": "application/json; charset=UTF-8"})
  logging.info("response status code: "+str(response.status_code))
  if response.status_code != 200:
    logging.error(response.text)

#store sent (adding to the existing file) and the rest
if os.path.exists(outputPath+'sent.json'):
  with open(outputPath+'sent.json') as sf:
    sent = json.load(sf)
else:
  sent = {}

unfinished = {}
for r in amibppbcatr:
  if int(r) in reallySentRuns:
    sent[r] = amibppbcatr[r]#each is sent once, i.e. there is no need to any existence checks
  else:
    unfinished[r] = amibppbcatr[r]
#pprint.pprint(sent)
#pprint.pprint(unfinished)

with open(outputPath+'sent.json', 'w') as sfile:
  json.dump(sent, sfile)
with open(outputPath+'unfinished.json', 'w') as ufile:
  json.dump(unfinished, ufile)

logging.info("output to store (unfinished) in: https://atlasdistributedcomputing-live.web.cern.ch/t0/unfinished.json")
logging.info("output to store (sent) in: https://atlasdistributedcomputing-live.web.cern.ch/t0/sent.json")
logging.info("sending script finished: "+str(datetime.now()))
