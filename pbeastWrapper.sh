#!/bin/bash
KRB5CCNAME=$(mktemp /tmp/krb5cc_$(id -u)_XXXXXX)
export KRB5CCNAME
trap "{ rm -f $KRB5CCNAME; }" EXIT
kinit -k -t /data/atlcrc/keytab atlcrc@CERN.CH
#
source /cvmfs/atlas.cern.ch/repo/sw/tdaq/tools/cmake_tdaq/bin/cm_setup.sh tdaq-10-00-00 > /dev/null 2>&1
#source /cvmfs/sft.cern.ch/lcg/views/LCG_101/x86_64-centos7-gcc11-opt/setup.sh
#PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos tdaq_python $1/pbeastPlot.py
PBEAST_SERVER_SSO_SETUP_TYPE=AutoUpdateKerberos tdaq_python $1/pbeast2monit.py
