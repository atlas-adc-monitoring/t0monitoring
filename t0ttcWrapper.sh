#!/bin/bash
#run it once a day - lets see if it is enough
if [ `date +"%H"` -eq 13 ]; then
HOME=/tmp
#get datasets from pyami for the latest data period of current year (to limit amount of data queried from AMI)
$1/t0ttc1PyamiWrapper.sh $1 > /dev/null
#add finished DAOD_PHYS task from BigPanda
$1/t0ttc2BigpandaWrapper.sh $1 > /dev/null
#add end of the run from PBeast
$1/t0ttc3PbeastWrapper.sh $1 > /dev/null
#add calibration loop and DDM data from MONIT
$1/t0ttc4MonitWrapper.sh $1
#send data to MONIT
$1/t0ttc5SendWrapper.sh $1
fi
