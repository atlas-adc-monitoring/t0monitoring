#!/bin/bash
KRB5CCNAME=$(mktemp /tmp/krb5cc_$(id -u)_XXXXXX)
export KRB5CCNAME
trap "{ rm -f $KRB5CCNAME; }" EXIT
kinit -k -t /data/atlcrc/keytab atlcrc@CERN.CH
#
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source $ATLAS_LOCAL_ROOT_BASE/user/atlasLocalSetup.sh
export PYAMI_CONFIG_DIR=/data/atlcrc/.pyami
lsetup pyami
python3 $1/t0ttc1Pyami.py
