#! /usr/bin/python3
import os
import subprocess
import json
from datetime import datetime, timezone
import logging
import pprint

if os.path.exists('/data/atlcrc/'):
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/t0/t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = '/eos/home-a/atlcrc/www/t0/'
else:
  logging.basicConfig(filename='t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = ''

logging.info("bigpanda script started: "+str(datetime.now()))


def queryBigpanda(datasetName):
  curlstring = "curl -s -b bigpanda.cookie.txt -H 'Accept: application/json' -H 'Content-Type: application/json'"+' "https://bigpanda.cern.ch/dataset/?datasetname='+datasetName+'*&json"'
  #print(curlstring)
  proc = subprocess.Popen(curlstring, stdout=subprocess.PIPE, shell=True)
  (out, err) = proc.communicate()
  outt = out.decode("utf-8")
  if outt != 'null':
    try:
      outtt = json.loads(out.decode("utf-8"))
      if outtt['nfiles'] == outtt['nfilesfinished']:#store only finished DAOD_PHYS tasks
        return outtt
      else:
        return []
    except ValueError:  # includes simplejson.decoder.JSONDecodeError
      logging.info("problematic BigPanda query output: "+str(outt))
      return []
  else:
    return []


with open(outputPath+'Py.json') as f:
  ami = json.load(f)

out = {}
formats = ['.RAW', '.AOD.', '.DAOD_PHYS.']
#copy content of Py.json into out dict
for r in list(ami):
  for d in list(ami[r]):
    for f in formats:
      if f in d:
        if r not in list(out):
          out[r] = {}
          out[r][d] = ami[r][d]
        else:
          out[r][d] = ami[r][d]
#pprint.pprint(out)

#add BigPanda info
for r in list(out):
  for d in list(out[r]):
    if '.DAOD_PHYS.' in d:
      q = queryBigpanda(d)
      if len(q) > 0:
        out[r][d]['taskid'] = q['jeditaskid']
        out[r][d]['taskdatasetname'] = q['datasetname']
        out[r][d]['taskstart'] = int(datetime.strptime(q['creationtime'], "%Y-%m-%dT%H:%M:%S").timestamp())
        out[r][d]['taskend'] = int(datetime.strptime(q['modificationtime'], "%Y-%m-%dT%H:%M:%S").timestamp())

with open(outputPath+'PyBp.json', 'w') as outfile:
  json.dump(out, outfile)

logging.info("output to store in: https://atlasdistributedcomputing-live.web.cern.ch/t0/PyBp.json")
logging.info("bigpanda script finished: "+str(datetime.now()))
