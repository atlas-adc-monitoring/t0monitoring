#! /usr/bin/python3
import os
import sys
import json
from datetime import datetime, timedelta, timezone
import logging
import pprint
import libpbeastpy

if os.path.exists('/data/atlcrc/'):
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/t0/t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = '/eos/home-a/atlcrc/www/t0/'
else:
  logging.basicConfig(filename='t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  outputPath = ''

logging.info("pbeast script started: "+str(datetime.now()))

now = datetime.now(tz=timezone.utc)
nowtimestamp = int(now.timestamp())
#before = now - timedelta(days=1)
#beforetimestamp = int(before.timestamp())

def getData(start):
  out = {}
  if len(sys.argv) > 1 and sys.argv[1] == 'test':
    #[QueryData(since=1651059409013572, until=1651067240838987, type=libpbeastpy.DataType.U32, is_array=False, data={'RunParams.EOR_RunParams': [DataPoint(ts=1651059409013572, value=419313), DataPoint(ts=1651065164157199, value=419324), DataPoint(ts=1651065880444623, value=419338), DataPoint(ts=1651067240838987, value=419341)]})]
    out = {1651059409013572: 419313, 1651065164157199: 419324, 1651065880444623: 419338, 1651067240838987: 419341}
  else:
    data = libpbeastpy.ServerProxy('https://atlasop.cern.ch')
    try:
      dresults = data.get_data('ATLAS', 'RunParams', 'run_number', 'RunParams.EOR_RunParams', False, start*10**6 ,nowtimestamp*10**6)
    except RuntimeError as e:
      logging.info(str(datetime.now())+'the query failed with: '+str(e))
      dresults = [['','','','',{'RunParams.EOR_RunParams':[]}]]
    for k in dresults[0][4]['RunParams.EOR_RunParams']:
      out[k[0]] = k[1]    
  return out

if os.path.exists(outputPath+'PyBp.json'):
  with open(outputPath+'PyBp.json') as f2:
    amibp = json.load(f2)
else:
  amibp = {}

#put the input into out dict
out = amibp

#get numbers of finished runs
finishedRuns = []
for r in list(amibp):
  for d in list(amibp[r]):
    if '.DAOD_PHYS.' in d:
      if 'taskend' in list(amibp[r][d]):
        if r not in finishedRuns:
          finishedRuns.append(r)
#print(finishedRuns)

#get creation time of RAW dataset for finished runs
finishedRunsRAWcreation = []
for rr in finishedRuns:
  for dd in list(amibp[rr]):
    if 'RAW' in dd:
      finishedRunsRAWcreation.append(int(datetime.strptime(amibp[rr][dd]['created'], "%Y-%m-%d %H:%M:%S").timestamp()))
#print(finishedRunsRAWcreation)
#print(min(finishedRunsRAWcreation)-100000)

#query PBeast with start of the query being the oldest RAW dataset
if len(finishedRunsRAWcreation) == 0:#if there are no datasets to check
  logging.info("no datasets to check")
else:
  qPb = getData(min(finishedRunsRAWcreation)-100000)
  for t in qPb:
    for r in amibp:
      if int(r) == qPb[t]:
        out[r]['EOR'] = str(t)[:-6]
#pprint.pprint(out)

with open(outputPath+'PyBpPb.json', 'w') as outfile:
  json.dump(out, outfile)

logging.info("output to store in: https://atlasdistributedcomputing-live.web.cern.ch/t0/PyBpPb.json")
logging.info("pbeast script finished: "+str(datetime.now()))
