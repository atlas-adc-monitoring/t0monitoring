#!/bin/bash
KRB5CCNAME=$(mktemp /tmp/krb5cc_$(id -u)_XXXXXX)
export KRB5CCNAME
trap "{ rm -f $KRB5CCNAME; }" EXIT
kinit -k -t /data/atlcrc/keytab atlcrc@CERN.CH
#
auth-get-sso-cookie -u https://bigpanda.cern.ch/oauth/login/cernoidc/ -o /data/atlcrc/t0monitoring/bigpanda.cookie.txt
python3 $1/t0ttc2Bigpanda.py
