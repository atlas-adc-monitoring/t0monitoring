#! /usr/bin/python3
import os
import re
import sys
#import math
import matplotlib.pyplot as plt
from datetime import datetime, timedelta, timezone
if len(sys.argv) > 1 and sys.argv[1] == 'test':
  import pprint
else:
  import libpbeastpy

print(f"plotting script started: {datetime.now()}")

if len(sys.argv) > 1 and sys.argv[1] == 'test':
  timerange = 86400
else:
  timerange = 86400
now = int(datetime.now(tz=timezone.utc).timestamp())
lastHour = int(datetime.now(tz=timezone.utc).replace(microsecond=0,second=0,minute=0).timestamp())#start at the beginning of an hour
yesterday = lastHour - timerange
#
#bins = '1m'
bins = '1h'
#plots = 'bar'
plots = 'line'

print("Current setting:")
print(f"-bar size: {bins}")
print(f"-plot style: {plots}")

def units(num):
  unit = ''
  if num < 1000:
    return num
  elif num < 10**6:
    return str(num)[:-3]+'k'
  elif num < 10**9:
    return str(num)[:-6]+'M'
  elif num < 10**12:
    return str(num)[:-9]+'G'
  elif num < 10**15:
    return str(num)[:-12]+'T'
  else:
    return num
  
def dataPlot(cdict, name):
  plt.clf()  # clear figure 
  #print(cdict)
  nmachines = len(cdict)
  width = 1/(nmachines+3)#The +3 is there to for a gap between time bins
  n = 0
  for i in cdict:
  #for i in ['pc-tdq-sfo-08.cern.ch-prod']:
    x = []
    xbar = []
    y = []
    for j in sorted(cdict[i]['data']):#sort the timestamps
      x.append(j)
      if plots == 'bar':
        #xbar.append(cdict[i]['index'][j])
        xbar.append(cdict[i]['index'][j]+n*width)
      y.append(cdict[i]['data'][j])
    #print(x)
    #print(xbar)
    #print(y)
    if plots == 'line':
      xx = []
      for k in x:
        xx.append(k-x[0])
      #print(xx)
      #print(xlabels)
      plt.xticks(xx,cdict[i]['labels'],rotation=90)
      #plt.xticks(xx,cdict[i]['labels'])
      plt.plot(xx,y, marker='o', markersize=3, label = i+' (max:'+str(units(cdict[i]['max']))+',avg:'+str(units(int(cdict[i]['avg'])))+',current:'+str(units(cdict[i]['current']))+')')
    if plots == 'bar':
      if n == int(nmachines//2):#floor division rounds down to nearest integer 
        #print(xbar)
        #print(cdict[i]['labels'])
        #print(y)
        #plt.xticks(xbar,cdict[i]['labels'])
        plt.xticks(xbar,cdict[i]['labels'],rotation=90)
      #print(y)
      plt.bar(xbar, y, width, label = i+' (max:'+str(units(cdict[i]['max']))+',avg:'+str(units(int(cdict[i]['avg'])))+',current:'+str(units(cdict[i]['current']))+')')
      n += 1
  plt.xlabel('Time')
  plt.ylabel('#files')
  plt.title(settings[name]['plotTitle']+' ('+bins+' average)',  x=0.6)
  plt.legend(bbox_to_anchor=(1.05, 0.5, 1, 0.5), loc=1, ncol=1, mode="expand", borderaxespad=0.)
  print(f"saving {settings[name]['attribute']} plot into {settings[name]['plotOutFile']}")
  plt.savefig(settings[name]['plotOutFile'], dpi=300, bbox_inches='tight', pad_inches=0.1)
  plt.close()
   
def processData(data, name):
  #use only last 24 hours
  dataSelected = {}
  for k in data:
    m = re.findall("pc-tdq-sfo-[0-9]+.cern.ch-prod", k)[0]
    dataSelected[m] = {}
    for j in data[k]:
      if int(str(j)[:-6]) > yesterday:
        dataSelected[m][j] = data[k][j]
        #print(datetime.fromtimestamp(int(str(j)[:-6])).strftime('%a,%H:%M'))
  #print(dataSelected)

  #the data are very non-equidistant - there are time ranges with multiple data points per minute followed by single data point in 5 days
  rebinned = {}
  if bins == '1m':
    bin1 = 60
  elif bins == '1h':
    bin1 = 3600
  else:
    bin1 = 1
  i = 0
  timebins = {}
  while (yesterday+i*bin1) < now:
    rebinned[i] = {}
    rebinned[i]['start'] = (yesterday+i*bin1)*10**6
    timebins[rebinned[i]['start']] = i
    rebinned[i]['end'] = rebinned[i]['start']+(bin1*10**6-0.000001)
    i += 1
  #print(timebins)
  #print(rebinned)

  #dict with time stamp at the beginning of the minute and value as list of data points which happened in that minute
  dataSelected1 = {}
  for d in dataSelected:
    dataSelected1[d] = {}
    for i in range(0, len(rebinned)):
      dataSelected1[d][rebinned[i]['start']] = []
      for j in dataSelected[d]:
        if rebinned[i]['start'] < j and j < rebinned[i]['end']:
          dataSelected1[d][rebinned[i]['start']].append(dataSelected[d][j])
          #print(datetime.fromtimestamp(int(str(j)[:-6])).strftime('%a,%H:%M'))
  #print(dataSelected1)

  #transforming the new dict into the same structure as dataSelected {machine:{timestamp:avgValue}}
  dataSelected2 = {}
  alltimes = []
  allindices = {}
  for d in dataSelected1:
    c = 0
    if plots == 'line':
      dataSelected2[d] = {'data':{}}
      for k in dataSelected1[d]:
        if len(dataSelected1[d][k]) > 0:
          dataSelected2[d]['data'][k] = int(sum(dataSelected1[d][k])/len(dataSelected1[d][k]))
          #print(datetime.fromtimestamp(int(str(k)[:-6])).strftime('%a,%H:%M'))
          if k not in alltimes:
            alltimes.append(k)
    if plots == 'bar':
      dataSelected2[d] = {'data':{}, 'index':{}}
      for k in dataSelected1[d]:
        if len(dataSelected1[d][k]) > 0:
          dataSelected2[d]['data'][k] = int(sum(dataSelected1[d][k])/len(dataSelected1[d][k]))
          dataSelected2[d]['index'][k] = c
          if k not in list(allindices):
            allindices[k] = c
          if k not in alltimes:
            alltimes.append(k)
        c += 1
  #print(alltimes)
  #print(allindices)
  #pprint.pprint(dataSelected2)

  #add all values needed for printing
  dataSelected3 = {}
  for d in dataSelected2:
    dataSelected3[d] = {'data':{}}
    #dataSelected3[d]['index'] = allindices
    dataSelected3[d]['index'] = timebins
    dataSelected3[d]['labels'] = []
    for t in sorted(timebins):
      if bins == '1m':
        dataSelected3[d]['labels'].append(datetime.fromtimestamp(int(str(t)[:-6])).strftime('%a,%H:%M'))
      if bins == '1h':
        dataSelected3[d]['labels'].append(datetime.fromtimestamp(int(str(t)[:-6])).strftime('%a,%H'))
      if t not in list(dataSelected2[d]['data']):
        #dataSelected3[d]['data'][t] = math.nan
        dataSelected3[d]['data'][t] = 0
    summ = 0
    maxx = []
    if len(dataSelected2[d]['data']) > 0:
      for k in dataSelected2[d]['data']:
        summ += dataSelected2[d]['data'][k]
        maxx.append(dataSelected2[d]['data'][k])
        dataSelected3[d]['data'][k] = dataSelected2[d]['data'][k]
        if 'index' in list(dataSelected2[d]):
          dataSelected3[d]['index'][k] = dataSelected2[d]['index'][k]
      dataSelected3[d]['avg'] = summ/len(dataSelected2[d]['data'])
      dataSelected3[d]['max'] = max(maxx)
      dataSelected3[d]['current'] = maxx[-1]
    else:
      dataSelected3[d]['avg'] = 0
      dataSelected3[d]['max'] = 0
      dataSelected3[d]['current'] = 0
  #pprint.pprint(dataSelected3)
  dataPlot(dataSelected3, name)

def getData(name):
  if len(sys.argv) > 1 and sys.argv[1] == 'test':
    ddata = {'RunParams.CastorScript-pc-tdq-sfo-03.cern.ch-prod.State': {1646368873947174: 0, 1646714421392628: 0}, 'RunParams.CastorScript-pc-tdq-sfo-04.cern.ch-prod.State': {1646368873947293: 0, 1646714425570927: 0}, 'RunParams.CastorScript-pc-tdq-sfo-05.cern.ch-prod.State': {1646368871315656: 0, 1646714421213963: 0}, 'RunParams.CastorScript-pc-tdq-sfo-06.cern.ch-prod.State': {1646368872151090: 0, 1646714424059569: 0}, 'RunParams.CastorScript-pc-tdq-sfo-07.cern.ch-prod.State': {1646368874713315: 0, 1646714423589624: 0}, 'RunParams.CastorScript-pc-tdq-sfo-08.cern.ch-prod.State': {1646368874713263: 0, 1646714423589628: 0}, 'RunParams.CastorScript-pc-tdq-sfo-11.cern.ch-prod.State': {1646380624490573: 27712, 1646380684524999: 27712, 1646380689528137: 27714, 1646380739557279: 27714, 1646380744560338: 27716, 1646380794590763: 27716, 1646380799594087: 27718, 1646380859626505: 27718, 1646380864629060: 27720, 1646380914658520: 27720, 1646380919661360: 27722, 1646380979694093: 27722, 1646380984696933: 27724, 1646381029721485: 27724, 1646381034724308: 27748, 1646381044729335: 27748, 1646381049732352: 27749, 1646714424754778: 27749}, 'RunParams.CastorScript-pc-tdq-sfo-12.cern.ch-prod.State': {1646380627539959: 27703, 1646380677567278: 27703, 1646380682570249: 27705, 1646380732599456: 27705, 1646380737602737: 27707, 1646380797638215: 27707, 1646380802640666: 27709, 1646380852669191: 27709, 1646380857671291: 27711, 1646380917704138: 27711, 1646380922706843: 27713, 1646380972732152: 27713, 1646380977734589: 27715, 1646381032764012: 27715, 1646381037766646: 27740, 1646714425649748: 27740}, 'RunParams.CastorScript-pc-tdq-sfo-13.cern.ch-prod.State': {1646380672064810: 27701, 1646380677067853: 27703, 1646380737104915: 27703, 1646380742107965: 27705, 1646380792138415: 27705, 1646380797141178: 27707, 1646380857176161: 27707, 1646380862179436: 27709, 1646380912206713: 27709, 1646380917209430: 27711, 1646380982243506: 27711, 1646380987246484: 27713, 1646381027268762: 27713, 1646381032271944: 27733, 1646381037274704: 27738, 1646714425649713: 27738}, 'RunParams.CastorScript-pc-tdq-sfo-14.cern.ch-prod.State': {1646380624491456: 27681, 1646380674519428: 27681, 1646380679522628: 27683, 1646380729552195: 27683, 1646380734554946: 27685, 1646380794591690: 27685, 1646380799594845: 27687, 1646380849621764: 27687, 1646380854624684: 27689, 1646380919662086: 27689, 1646380924664503: 27691, 1646380974692716: 27691, 1646380979695245: 27693, 1646381029722235: 27693, 1646381034724979: 27696, 1646381039727115: 27718, 1646714425088187: 27718}, 'RunParams.CastorScript-pc-tdq-sfo-15.cern.ch-prod.State': {1646380672564285: 27511, 1646380732599561: 27511, 1646380737602530: 27513, 1646380787632152: 27513, 1646380792635189: 27515, 1646380852668967: 27515, 1646380857670856: 27517, 1646380907697646: 27517, 1646380912700307: 27519, 1646380977734605: 27519, 1646380982737001: 27521, 1646381032763837: 27521, 1646381037766619: 27523, 1646381077790763: 27523, 1646381082793502: 27546, 1646714423154771: 27546}, 'RunParams.CastorScript-pc-tdq-sfo-16.cern.ch-prod.State': {1646380674206191: 27516, 1646380724236596: 27516, 1646380729239654: 27518, 1646380789279326: 27518, 1646380794282785: 27520, 1646380844311093: 27520, 1646380849313965: 27522, 1646380909349355: 27522, 1646380914352508: 27524, 1646380964381506: 27524, 1646380969384693: 27526, 1646381029419266: 27526, 1646381034422273: 27528, 1646381079450637: 27528, 1646381084454533: 27551, 1646714422730400: 27551}}
  else:
    data = libpbeastpy.ServerProxy('https://atlasop.cern.ch')
    #
    ddata = {}
    sfomachines = []
    try:
      sfomachines = data.get_objects(settings[name]['partition'], settings[name]['class'], settings[name]['attribute'])
    except RuntimeError as e:
      print('Getting a list of machines failed with:')
      print(e)
    for m in sfomachines:
      ddata[m] = {}
      for i in data.get_data(settings[name]['partition'], settings[name]['class'], settings[name]['attribute'], m, False, yesterday*10**6 ,now*10**6)[0][4][m]:
        ddata[m][i[0]] = i[1]
  return ddata

settings = {}
settings['filesToTransfer'] = {}
settings['filesToTransfer']['partition'] = 'initial'
settings['filesToTransfer']['class'] = 'CastorScriptState'
settings['filesToTransfer']['attribute'] = 'files_to_be_copied'
settings['filesToTransfer']['plotTitle'] = 'Files to Transfer'
if os.path.exists('/data/atlcrc/'):
  settings['filesToTransfer']['plotOutFile'] = '/eos/home-a/atlcrc/www/images/filesToTransfer.png'
else:
  settings['filesToTransfer']['plotOutFile'] = 'filesToTransfer.png'
settings['ongoingCopies'] = {}
settings['ongoingCopies']['partition'] = 'initial'
settings['ongoingCopies']['class'] = 'CastorScriptState'
settings['ongoingCopies']['attribute'] = 'files_being_copied'
settings['ongoingCopies']['plotTitle'] = 'Ongoing copies'
if os.path.exists('/data/atlcrc/'):
  settings['ongoingCopies']['plotOutFile'] = '/eos/home-a/atlcrc/www/images/ongoingCopies.png'
else:
  settings['ongoingCopies']['plotOutFile'] = 'ongoingCopies.png'

#
for p in ['filesToTransfer', 'ongoingCopies']:
  processData(getData(p), p)

print(f"plotting script finished: {datetime.now()}")
