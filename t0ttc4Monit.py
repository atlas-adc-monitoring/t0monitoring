#! /usr/bin/python3
import os
import re
import yaml
import json
import math
import requests
from datetime import datetime, timedelta, timezone
import logging
import pprint

def grafana_ES(api, query):
  #print(query)
  #secrets = yaml.load(open(secretsPath+'send_CRC.secrets'))
  secrets = yaml.load(open(secretsPath+'send_CRC.secrets'), Loader=yaml.BaseLoader)
  try:
    headers = {}
    headers['Authorization'] = 'Bearer %s' % secrets['Monit Grafana']['token']
    headers['Content-Type'] = 'application/json'
    headers['Accept'] = 'application/json'
    base = "https://monit-grafana.cern.ch"
    url = api
    request_url = "%s/%s" % (base, url)
    r = requests.post(request_url, headers=headers, data=query)
    #print(r.text)
    return json.loads(r.text)
  except Exception as e:
    print("unable to query grafana with query: "+query)
    print("error message: "+str(e))
    return {'responses':[{'hits':{'hits':[]}}]}


def run_ddm_dashboard_query_ES(query):
  api = "api/datasources/proxy/8736/_msearch?max_concurrent_shard_requests=5"
  data = grafana_ES(api, query)
  #print(data)
  #pprint.pprint(data['responses'][0]['hits']['hits'])
  #print(len(data['responses'][0]['hits']['hits']))
  if len(data['responses'][0]['hits']['hits']) > 0:
    return data['responses'][0]['hits']['hits']
  else:
    return []


def run_completed_job_dashboard_query_ES(query):
  api = "api/datasources/proxy/7684/_msearch?max_concurrent_shard_requests=5"
  data = grafana_ES(api, query)
  #pprint.pprint(data)
  if len(data['responses'][0]['hits']['hits']) > 0:
    return data['responses'][0]['hits']['hits']
  else:
    return 'N/A'

if os.path.exists('/data/atlcrc/'):
  logging.basicConfig(filename='/eos/home-a/atlcrc/www/t0/t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  secretsPath = '/data/atlcrc/ADC-live/'
  outputPath = '/eos/home-a/atlcrc/www/t0/'
else:
  logging.basicConfig(filename='t0ttc.log', filemode='a', format='%(message)s', level=logging.INFO)
  secretsPath = ''
  outputPath = ''

logging.info("monit script started: "+str(datetime.now()))

end = int(datetime.utcnow().replace(tzinfo=timezone.utc).timestamp()*1000)
#print('end: '+datetime.utcnow().replace(tzinfo=timezone.utc).strftime("%Y-%m-%d %H:%M:%S"))

if os.path.exists(outputPath+'PyBpPb.json'):
  with open(outputPath+'PyBpPb.json') as f2:
    amibppb = json.load(f2)
else:
  amibppb = {}

#put the input into out dict
out = amibppb

#get finished runs
rnList = []
for r in list(amibppb):
  for d in list(amibppb[r]):
    if '.DAOD_PHYS.' in d:
      if 'taskend' in list(amibppb[r][d]):
        if r not in rnList:
          rnList.append(r)
rstart = {}
for r in rnList:
  if 'EOR' in list(amibppb[r]):
    #rstart[r] = int(amibppb[r]['EOR'])*1000
    rstart[r] = (int(amibppb[r]['EOR'])-1000000)*1000
  else:#if EOR is not defined, derive the start of time window from RAW dataset creation
    for d in amibppb[r]:
      if 'RAW' in d:
        rstart[r] = (int(datetime.strptime(amibppb[r][d]['created'], "%Y-%m-%d %H:%M:%S").timestamp())-1000000)*1000
  #print('start of '+r+': '+str(datetime.fromtimestamp(int(rstart[r])/1000)))
#(start)

#print(rnList)
#############################Calibration loop#############################

#CTIME=data.taskcreationtime
#MTIME=data.taskmodificationtime
#run_number=data.runnr
for rn in rnList:
  #print(rn)
  #the query has limit on number of entries (10000), i.e. if there is more than 10k jobs, I need to do the query again but this time with different start
  loopLimit = 1
  endings = []
  done = []#cache list
  before = 0#just to pass first loop
  after = 1#just to pass first loop
  mt = []
  ct = {}#{jobname:creationtime}
  ctl = []#list of creation times
  while after > before:
    if len(endings) == 0:
      lastDate = rstart[rn]
    else:
      before = lastDate
      lastDate = max(endings)
      after = lastDate
    #print(lastDate)
    #print(datetime.utcfromtimestamp(int(lastDate/1000)))
    #print(end)
    #print(datetime.utcfromtimestamp(int(end/1000)))
    #print()
    q = 'data.runnr: '+str(rn)
     #query sorting ascending - to get the oldest in the first step then newer - moving towards current timestamp
    t0query = '{\"search_type\":\"query_then_fetch\",\"ignore_unavailable\":true,\"index\":[\"monit_prod_atlasjm_enr_completed*\"]}\n{\"size\":10000,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"metadata.timestamp\":{\"gte\":'+str(lastDate)+',\"lte\":'+str(end)+',\"format\":\"epoch_millis\"}}},{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"'+q+'\"}}]}},\"sort\":[{\"metadata.timestamp\":{\"order\":\"asc\",\"unmapped_type\":\"boolean\"}},{\"_doc\":{\"order\":\"asc\"}}],\"script_fields\":{}}\n'
    #print(t0query)
    t0rawdata = run_completed_job_dashboard_query_ES(t0query)
    #with open(outputPath+'428700.json', 'w') as ofile:
    #  json.dump(t0rawdata, ofile)
    #with open(outputPath+'428700.json') as f2:
    #  t0rawdata = json.load(f2)
    #pprint.pprint(t0rawdata)
    if t0rawdata == 'N/A':
      endings.append(end)
    else:
      for i in t0rawdata:
        endings.append(i['_source']['metadata']['timestamp'])
        done.append(i)
  #get beginning of bulk processing
  for j in done:
    #print(j['_source']['data']['jobname'])
    #pprint.pprint(j)
    if re.search(".*[.]RAW[.]f.*[.]recon[.].*", j['_source']['data']['jobname']):#start of bulk - any Tier-0 job for given run with jobname like '%.RAW.f%.recon.%'
      #print(j)
      ct[j['_source']['data']['jobname']] = str(j['_source']['data']['taskcreationtime'])[:-3]
      ctl.append(str(j['_source']['data']['taskcreationtime'])[:-3])
  if len(ctl) > 0:
    ctim = min(ctl)#first RAW.f*.recon job created
    ctag = ''
    for u in ct:
      if ct[u] == ctim:
        ctag = str(u.split('.f')[1].split('.')[0])#jobs should have the same tag, i.e. there should be no need for recording them all and extracting uniques
    configtag = 'f'+ctag
    #print(configtag)
    #get the end of bulk processing
    for i in done:
      if re.search(".*[.]"+configtag+".*", i['_source']['data']['jobname']):#end of bulk - any Tier-0 job for given run with the configtag same as the first job and jobname like '%.configtag%'
        mt.append(str(i['_source']['data']['taskmodificationtime'])[:-3])
        #print(i['_source']['data']['jobname'])
        #print(i['_source']['data']['taskmodificationtime'])
    out[rn]['ctime'] = min(ctl)#take ctime as taskcreationtime of the first Tier-0 job
    out[rn]['mtime'] = max(mt)#take mtime as mtime of the last T0 job
    logging.info('run number:'+str(rn)+', ctime:'+out[rn]['ctime']+' ('+str(datetime.fromtimestamp(int(out[rn]['ctime'])))+'), mtime:'+out[rn]['mtime']+' ('+str(datetime.fromtimestamp(int(out[rn]['mtime'])))+')')

#############################Transfers#############################

#https://technoteshelp.com/elasticsearch-difference-between-term-match-phrase-and-query-string/
#-term query matches a single term as it is : the value is not analyzed
#-match_phrase query will analyze the input if analyzers are defined for the queried field and find documents matching the following criteria:
#--all the terms must appear in the field
#--they must have the same order as the input value
#--there must not be any intervening terms, i.e. be consecutive (potentially excluding stop-words but this can be complicated)
#-query_string query search, by default, on a _all field which contains the text of several text fields at once. On top of that, its parsed and supports some operators (AND/OR…), wildcards and so on (see related syntax).
#--Unlike the match_phrase, the terms obtained after analysis dont have to be in the same order, unless the user has used quotes around the input.
#
#dataset = 'data22_13p6TeV.00428580.physics_Background.merge.RAW'
#query = '{\"search_type\":\"query_then_fetch\",\"ignore_unavailable\":true,\"index\":[\"monit_prod_ddm_enr_transfer*\"]}\n{\"size\":10000,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"metadata.timestamp\":{\"gte\":'+start+',\"lte\":'+end+',\"format\":\"epoch_millis\"}}},{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"data.event_type: (transfer-failed OR transfer-done)\"}}],\"must\":[{\"match_phrase\":{\"data.activity\":{\"query\":\"T0 Export\"}}},{\"match_phrase\":{\"data.dataset\":{\"query\":\"'+dataset+'\"}}}]}},\"sort\":[{\"metadata.timestamp\":{\"order\":\"desc\",\"unmapped_type\":\"boolean\"}},{\"_doc\":{\"order\":\"desc\"}}],\"script_fields\":{}}\n'

formats = ['.RAW', 'AOD.']
for r in rnList:
  for d in list(amibppb[r]):
    for f in formats:
      if f in d:
        #print(d)
        #the query has limit on number of entries (10000), i.e. if there is more than 10k files, I need to do the query again but this time with different start
        loopLimit = 1
        if int(amibppb[r][d]['nfiles']) > 9999:
          loopLimit = math.ceil(int(amibppb[r][d]['nfiles'])/10000)#how many times I need to loop to cover all files
        #print(loopLimit)
        endings = []
        done = {}#cache dict for transfers - if there needs to be more than one query - for each run and dataset, there is {link:list_of_files, link-timestamp: list_of_timestamps} - the list of files is there to handle duplicates
        i = 0
        while i < loopLimit:
          i += 1#loop counter
          #
          for l in list(done):
            if 'timestamp' in l:
              endings.append(max(done[l]))
          if len(endings) == 0:
            lastDate = rstart[r]
          else:
            lastDate = max(endings)
          #print(lastDate)
          #query sorting ascending - to get the oldest in the first step then newer - moving towards current timestamp
          query = '{\"search_type\":\"query_then_fetch\",\"ignore_unavailable\":true,\"index\":[\"monit_prod_ddm_enr_transfer*\"]}\n{\"size\":10000,\"query\":{\"bool\":{\"filter\":[{\"range\":{\"metadata.timestamp\":{\"gte\":'+str(lastDate)+',\"lte\":'+str(end)+',\"format\":\"epoch_millis\"}}},{\"query_string\":{\"analyze_wildcard\":true,\"query\":\"data.event_type: transfer-done AND data.src_experiment_site: CERN-PROD AND data.dst_tier:1\"}}],\"must\":[{\"match_phrase\":{\"data.activity\":{\"query\":\"T0 Export\"}}},{\"match_phrase\":{\"data.dataset\":{\"query\":\"'+d+'\"}}}]}},\"sort\":[{\"metadata.timestamp\":{\"order\":\"asc\",\"unmapped_type\":\"boolean\"}},{\"_doc\":{\"order\":\"asc\"}}],\"script_fields\":{}}\n'
          #outddm[r][f+'transfer'] = run_ddm_dashboard_query_ES(query)
          #print(len(outddm[r][f+'transfer']))
          for t in run_ddm_dashboard_query_ES(query):
            #dataset = t['_source']['data']['dataset']
            fname = t['_source']['data']['name']
            transferFinishedAt = t['_source']['data']['transferred_at']
            #make channels as site to site because there are examples like:
            #data22_13p6TeV.00428855.physics_Main.merge.AOD.f1248_m2112
            #4143 files
            #CERN-PROD_TZDISK-BNL-OSG2_DATADISK: 4139
            #CERN-PROD_DATADISK-BNL-OSG2_DATADISK: 4
            link = t['_source']['data']['src_experiment_site']+'-'+t['_source']['data']['dst_experiment_site']
            if link not in list(done):
              done[link] = []
              done[link+'-timestamp'] = []
              if fname not in done[link]:#to handle duplicates from fake transfers
                done[link].append(fname)
                done[link+'-timestamp'].append(transferFinishedAt)
            else:
              if fname not in done[link]:#to handle duplicates from fake transfers
                done[link].append(fname)
                done[link+'-timestamp'].append(transferFinishedAt)
        lastTimestamp = 0
        for l in list(done):
          if 'timestamp' in l:
            lastFromLink = max(done[l])
            if lastTimestamp < lastFromLink:
              lastTimestamp = lastFromLink
        out[r][d]['transferend'] = str(lastTimestamp)[:-3]
#pprint.pprint(out)

with open(outputPath+'PyBpPbCaTr.json', 'w') as ofile:
  json.dump(out, ofile)

logging.info("output to store in: https://atlasdistributedcomputing-live.web.cern.ch/t0/PyBpPbCaTr.json")
logging.info("monit script finished: "+str(datetime.now()))
